package code.snippet

import net.liftweb.common.Full
import net.liftweb.util.PassThru
import omniauth.{AuthInfo, Omniauth}
import net.liftweb.util.Helpers._

/**
 * Created by j2 on 28-02-15.
 */
class HomeSnippet {

  def render = {
    "*" #> PassThru
  }

}
